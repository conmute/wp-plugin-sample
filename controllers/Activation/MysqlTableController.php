<?php
namespace ConmuteSample\Activation;

class MysqlTableController extends \ConmuteSample\ActivationController {
	private $table_prefix;
	public function __construct() {
		parent::__construct();
		global $wpdb;
		$this->table_prefix = $wpdb->prefix.$this->config['plugin']['prefix'].'_';   
		$this->optionsController = new OptionsController($this->config);
	}
	/** Launches on plugin activation
	 * Also i aded this function to hook every time when script lauches
	 * As you see there are Creation and Update block
	 * inside the statement here
	 * 	if($res) { statement }
	 * you can add prefered actions for you
	 */
	public function plugin_activation() {
		// install_or upgrade tables and params
		foreach( $this->config['datatables'] as $key => $value ) { // go through each table in main config file
			//mail('roman@c-o-s.name','csample_activation','FN used: pased through specified point '.$key.'<pre>'.print_r($value, true).'</pre>');
			$table_name = $this->table_prefix.$key;
			$installed_ver_key = $this->config['plugin']['prefix'].'_table_'.$table_name.'_version';
			$installed_ver = get_option( $installed_ver_key );

			if( $installed_ver == false ) { // CREATION block
				// if current version is not installed - install it
				$res = $this->dbTableCreateOrUpdate($table_name, $value['sql']['insert-update']);
				if($res) {
					// add table version option to WP database
					add_option( $installed_ver_key, $value['version'], '', 'no' );
					parent::plugin_post_activation();
				}
			} else {
				$this->plugin_update();
			}
		}
		
	}
	/** Launches for plugin updating
	 */
	public function plugin_update() {
		if ($installed_ver != $value['version']) { // UPDATE block
			// if current version is outdated - update it
			$res = $this->dbTableCreateOrUpdate($table_name, $value['sql']['insert-update']);
			if($res) {
				// update table version option to WP database
				update_option( $installed_ver_key, $value['version'] );
				parent::plugin_post_update();
			}
		}
	}
	/** Launches on deactivation plugin
	 */
	public function plugin_deactivation() {
		$this->plugin_uninstall(); // - develop mode, unisntall action on deactivating plugin, avoiding from uneaded deletion of plugin files
	}
	/** Luanches when plugin is uninstalled 
	 * (after succes plugin files are deleted from wp source foulder)
	 */
	public function plugin_uninstall() {
		// delete all DB tables and params with DB tables versions
		foreach( $this->config['datatables'] as $key => $value ) {
			$table_name = $this->table_prefix.$key;
			$installed_ver_key = $this->config['plugin']['prefix'].'_table_'.$table_name.'_version';
			if( get_option( $installed_ver_key ) ) {
				$e = $this->dbTableDelete($table_name, $value['sql']['delete']);
				if($e) {
					delete_option( $installed_ver_key );
				}
			}
		}
		
		parent::plugin_post_uninstall();
	}
	/** Deletes datatable from wordpress database
	 * Supports multiple qeries!
	 */
	private function dbTableDelete($table_name, $table_sql) {
		global $wpdb;
		$sql = str_replace( // change `__tablename__` to `wp_cossu_providers` for example
			'__tablename__',
			$table_name,
			$table_sql
		);
		$queries = explode(';',$sql);
		$success = true;
		foreach($queries as $querie) {
			if(strlen(trim($querie)) != 0) {
				$e = $wpdb->query($querie);
				if(!$e) {
					$success = false;
				}
			}
		}
		return $success;
	}
	/** Creates or updates database table with help of dbDelta functions
	 * The sql sended here must be writened for that function (dbDelta)
	 * For more info look here
	 * 	http://codex.wordpress.org/Creating_Tables_with_Plugins
	 * @see http://codex.wordpress.org/Creating_Tables_with_Plugins
	 */
	private function dbTableCreateOrUpdate($table_name, $table_sql) {
		global $wpdb;
		$sql = str_replace( // change `__tablename__` to `wp_cossu_providers` for example
			'__tablename__',
			$table_name,
			$table_sql
		);
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		$res = dbDelta($sql);
		$link = $wpdb->get_row("SHOW TABLES LIKE '".$table_name."';", ARRAY_N);
		return (count($link)==1)?True:False;
	}
}