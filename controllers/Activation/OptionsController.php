<?php
namespace ConmuteSample\Activation;

class OptionsController extends \ConmuteSample\ActivationController {
    public function __construct() {
    	parent::__construct();
    }
	/** Creates wp options, that can be used further in plugin
	 */
	public function prepareWPOptions() {
		/**
		 * Look at
		 * http://codex.wordpress.org/Function_Reference/add_option
		 * 
		 * add_option( $option_Name, $value, $deprecated, $autoload );
		 */
		add_option($this->config['plugin']['prefix'] . "sample_option", 'Sample value');
	}
	/** Deletes wp options
	 */
	public function deleteWPOptions() {
		/**
		 * Look at
		 * http://codex.wordpress.org/Function_Reference/delete_option
		 * 
		 * delete_option( $option_Name );
		 */
		delete_option($this->config['plugin']['prefix'] . "sample_option");
	}
	/** Updates wp options
	 */
	public function updateWPOptions() {
	}

	public static function f() {
		return new OptionsController();
	}
}