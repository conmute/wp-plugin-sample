<?php
namespace ConmuteSample;

class AdminPageController extends Controller {
    public function __construct() {
		parent::__construct();

		add_action("admin_menu", Array( &$this, "register_menu") );
    }
	/**
	 * Used by add_action hook, add menus into admin panel
	 */
	public function register_menu() {
		add_options_page(
			"Conmute Sample: налаштування",
			"Conmute Sample",
			"manage_options", 
			PLUGIN_PREFIX . "-conmute-sample", 
			Array( &$this, "plugin_setting_page" )
		);
	}
	/**
	 * Show setting in plugin page
	 * @return string - HTML code, containes neaded to edit plugin settings
	 */
	public function plugin_setting_page() {
	    if( $submitted = isset( $_POST['submit'] )) {
	        $optionName = PrexifierHelper::optionName('sample_option');
	        update_option($optionName, ($_POST[$optionName])?$_POST[$optionName]:'-1' );
	    }
	   // echo MiscHelper::optionName('sample_option');
	    ViewHelper::admin('settings', array(
	        'submitted' => $submitted,
	    ));
	}
}