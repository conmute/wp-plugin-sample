<?php
namespace ConmuteSample;
use ConmuteSample;

class ActivationController extends Controller {

	public function __construct() {
		parent::__construct();
	}

	public static function activation() {
		$TableCreator = new Activation\MysqlTableController();
		$TableCreator->plugin_activation();
	}

	public static function update() {
		$TableCreator = new Activation\MysqlTableController();
		$TableCreator->plugin_update();
	}

	public static function uninstall() {
		$TableCreator = new Activation\MysqlTableController();
		$TableCreator->plugin_uninstall();
	}

	public function plugin_post_activation() {
		Activation\OptionsController::prepareWPOptions();
	}

	public function plugin_post_update() {
		Activation\OptionsController::updateWPOptions();
	}

	public function plugin_post_uninstall() {
		Activation\OptionsController::deleteWPOptions();
	}

}