<?php

    namespace ConmuteSample;
    
    spl_autoload_register(__NAMESPACE__ . '\\autoload');

    global $plugin_dir;
    $plugin_dir = plugin_dir_path(dirname(__FILE__));

    function autoload($className) {
        global $plugin_dir;
        $className = ltrim($className, '\\');
        
        // skip other classes
        if(strpos($className, __NAMESPACE__) !== 0)
            return;
        $className = str_replace(__NAMESPACE__, '', $className);
        
        $folder = 'inc';
        
        // this should done with monads maybe. But i still don't know a lot about them, so i will leave this for future
        if (strpos($className, 'Controller') === FALSE) {
            if (strpos($className, 'Helper') === FALSE) {
                if (strpos($className, 'Model') === FALSE) {
                    $folder = 'inc';
                } else {
                    $folder = 'models';
                }
            } else {
                $folder = 'helpers';
            }
        } else {
            $folder = 'controllers';
        }

        $path = $plugin_dir . $folder . 
            str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';

        // error_log('~here: '.$path);

        require_once($path);
    }
