<?php
namespace ConmuteSample;
use ConmuteSample;
global $theC;
$theC = new Conmute();
// ob_start();
// var_dump($theC);
// error_log(ob_get_clean());
function c() {
	global $theC;
	// $theC = new Conmute();
	return $theC;
}

$config = c()->getConfig('main');

define(__NAMESPACE__ . '\\PLUGIN_PATH', plugin_dir_path(dirname(__FILE__)));
define(__NAMESPACE__ . '\\PLUGIN_PREFIX', c()->byPath($config, 'plugin/prefix'));