<?php
namespace ConmuteSample;
use ConmuteSample;
class Conmute {
	protected $config = array();
	public function getConfig($name) {
		$confFolder = dirname(dirname(__FILE__)) . '/configs/';

		if (isset($this->config[$name])) {
			return $this->config[$name];
		} else if (file_exists($filename = $confFolder . $name . '.php')) {
			$this->config[$name] = require($filename);
			return $this->config[$name];
		} else {
			return NULL;
		}
	}
	public function optionName($name) {
		return PrexifierHelper::optionName($name);
	}
	/**
	 * Get value of an array by using "root/branch/leaf" notation
	 *
	 * @param array $array   Array to traverse
	 * @param string $path   Path to a specific option to extract
	 * @param mixed $default Value to use if the path was not found
	 * @return mixed
	 */
	function byPath(array $array, $path, $default = null)
	{
		// specify the delimiter
		$delimiter = '/';
	
		// fail if the path is empty
		if (empty($path)) {
			throw new Exception('Path cannot be empty');
		}
	
		// remove all leading and trailing slashes
		$path = trim($path, $delimiter);
	
		// use current array as the initial value
		$value = $array;
	
		// extract parts of the path
		$parts = explode($delimiter, $path);

		// loop through each part and extract its value
		foreach ($parts as $part) {
			if (isset($value[$part])) {
				// replace current value with the child
				$value = $value[$part];
			} else {
				// key doesn't exist, fail
				return $default;
			}
		}
	
		return $value;
	}
}