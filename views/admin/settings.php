<?php
	namespace CplQuestionnaire;
	use CplQuestionnaire;
?>
<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div><h2 title="www.conmute.com Sample Options">www.conmute.com Приклад сторінки налаштування</h2>
	<?php
		if( $submitted ):
			?>
			<div id="setting-error-settings_updated" class="updated settings-error"> 
				<p><strong>Налаштування збережено.</strong></p>
			</div>
		    <?php
		endif;
	?>
	<form action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="POST">
		<table class="form-table"><tbody>
			<tr valign="top">
				<th scope="row">Довільний набір опцій:</th>
				<td>
					<fieldset>
						<legend class="screen-reader-text">
							<span>Назва довільної змінної: </span>
						</legend>
							
						<?php
							$args = Array(
								'show_option_none' => 'Виберіть сторінку',
								'name' => c()->optionName('sample_option'),
								'selected' => get_option(c()->optionName('sample_option')),
							);
							wp_dropdown_pages( $args );
						?>
						
						<p class="description">
							Примітка: Довільний опис, на даний момент записуватиметься в змінну ідентифікатор обраної сторінки.
						</p>
					</fieldset>
				</td>
			</tr>
		</tbody></table>
		<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="Зберегти зміни"></p>
	</form>
</div>