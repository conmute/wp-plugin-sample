<?php
$dirname = dirname( dirname( __FILE__ ) );
$theme = 'default';
$pluginUrl = plugins_url() . '/' . basename( $dirname );

$options = Array();
// plugin ...
if(true) { //... VARIABLES
	/**
	 * all plugin related variables
	 */
	$options['plugin'] =  Array(
		'url' => $pluginUrl,
		'path' => $dirname,
		'theme' => $theme,
		'prefix' => 'csmpl',
	);
}
if(true) { //... database TABLES configuration 	
	/**
	 * here we initialize all DataTables in such structure:
	 * 
	 'datatables' => Array(
		'<table name>' => Array(
			'sql' => Array(
				'insert-update' => "CREATE TABLE __tablename__ (
					id int(11) NOT NULL AUTO_INCREMENT,
					PRIMARY KEY  (id)
				);",
				'delete' => "DROP TABLE IF EXISTS __tablename__;"
			),
			'version' => '0.1'
		),
		...
	 ),
	 * where 
	 * 	<table name> - is your DB table name
	 * 	__tablename__ - is very nesessary for creating-updating & deleting, this will be replaced to <table name>
	 * 
	 * The ['sql']['insert-update'] query must be formated for dbDelta function, there are plenty of rules for that
	 * Look them here:
	 * 	http://codex.wordpress.org/Creating_Tables_with_Plugins
	 */
	$options['datatables'] = Array(
		'sample' => Array(
			'sql' => Array(
				'insert-update' => "CREATE TABLE __tablename__ (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					hashcode varchar(32) NOT NULL,
					data longblob NOT NULL,
					updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
					UNIQUE KEY id (id),
					UNIQUE KEY hashcode (hashcode)
				);",
				// UNIQUE KEY  hashcode (hashcode)
				'delete' => "DROP TABLE IF EXISTS `__tablename__`;"
			),
			'version' => '0.1'
		),
		// .. more tables goes down here
	);
}
return $options;
?>