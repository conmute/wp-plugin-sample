<?php
namespace ConmuteSample;

class ViewHelper {
    /**
     * requires view part if exists
     * 
     * @param $path - relative file path or name from admin view folder
     * @param $args - arguments as variables that must be visible for selected view
     */
    public static function admin($path, $args) {
        if( is_array($args) ) {
            extract($args);
        }
        $fileName = PLUGIN_PATH . 'views/admin/' . $path . '.php';
        if(file_exists($fileName)) {
            require($fileName);
        }
    }
}
?>