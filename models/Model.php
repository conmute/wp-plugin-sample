<?php

namespace ConmuteSample;

abstract class Model {
	protected $config;
	protected $table_name = null;
	protected $table_prefix = null;
	protected $prefix = null;
	function __construct() {
		$this->config = c()->getConfig('main');

		if ($this->table_name) {
			global $wpdb;
			$this->table_prefix = $wpdb->prefix . $this->config['plugin']['prefix'].'_';
		}
	}

	protected function tableName($name = null, $plugin = true) {
		if($name !== null) {
			global $wpdb;
			return $plugin ? $this->table_prefix . $name : $wpdb->prefix . $name;
		}
		if ($this->table_name && $this->table_prefix) {
			return $this->table_prefix . $this->table_name;
		} else return null;
	}

	public function getAll($query_options, $column = null) {
		global $wpdb;

		if (!$this->tableName()) return null;
		
		if(isset($query_options['search_key'])) {
			$query_options['where'] = sprintf( "%s like '%s'", $column, "%" . $query_options['search_key'] . "%");
		}

		$fields = implode($query_options['fields'], ", ");
		$fields = $fields?$fields:'*';

		$joins = (isset($query_options['joins']) ? $query_options['joins'] : "");
		$where = (isset($query_options['where']) ? " where "		. $query_options['where'] : "");
		$order = (isset($query_options['order']) ? " order by " 	. $query_options['order'] : "");

		return $wpdb->get_results("
			select {$fields} from {$this->tableName()}
				{$joins}
				{$where}
				{$order}
			;
		", ARRAY_A);
	}

	public function getSingle($where) {
		if (!$this->tableName()) return null;

		if (FALSE === is_int($where) && FALSE === is_string($where)) {
			trigger_error('getSingle expected Argument 1 to be Integer or a String', E_USER_WARNING);
		}

		global $wpdb;
		return $wpdb->get_row("select * from " . $this->tableName() . " where " . $where, ARRAY_A);
	}

	public function insert(Array $data) {
		if (!$this->tableName()) return false;

		global $wpdb;
		return $wpdb->insert( $this->tableName(), $data );
	}

	public function delete(Array $where) {
		if (!$this->tableName()) return false;
		
		global $wpdb;
		return $wpdb->delete( $this->tableName(), $where );
	}

	public function update(Array $data, Array $where) {
		if (!$this->tableName()) return false;

		global $wpdb;
		return $wpdb->update( $this->tableName(), $data, $where );
	}

}

?>