<?php
    /*
    Plugin Name: conmute.com PHP Code sample plugin
    Description: WordPress sample plugin basing on MVC model Requires php 5.3 and more. Uses: namespaces
    Version: 0.1
    Author: Roman M. Kos
    Author URI: http://www.conmute.com/
    License: Free for use!
    */
    namespace ConmuteSample;
    use ConmuteSample;
    
    // plugin class autoloader
    include('inc/autoloader.php');
    /**
     * Global plugin functions
     */
    include('inc/globals.php');

    /**
     * Pudge HOOKS :) YAYAYAYAA
     */
    $AdminPage = new AdminPageController();
    // $ObjectManagerController = new ObjectManagerController();
    
    // plugin registration
    register_activation_hook(__FILE__, array('\ConmuteSample\ActivationController', "activation"));
    // register_deactivation_hook(__FILE__, array('\ConmuteSample\ActivationController', "deactivation"));
    register_deactivation_hook( __FILE__, array('\ConmuteSample\ActivationController', "uninstall"));
    register_uninstall_hook( __FILE__, array('\ConmuteSample\ActivationController', "uninstall"));

?>